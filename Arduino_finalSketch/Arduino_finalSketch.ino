#include <SoftwareSerial.h>
#include <DallasTemperature.h>
#include "HX711.h"
#include <EEPROM.h>
#include "EEPROMAnything.h"
HX711 cell(5, 4); 
static long mgkval = 8235375;
long mgkval2; 
int temp_sensor = 11;       // Pin DS18B20 Sensor is connected to

int temperature = 0;      //Variable to store the temperature in

const int trigPin = 9;
const int echoPin = 10;
long duration;
int distance;
SoftwareSerial esp8266(2,3); 
OneWire oneWirePin(temp_sensor);

DallasTemperature sensors(&oneWirePin);
long val = 0;
long val2 = 0;

void setup() {
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT); 
  Serial.begin(115200);
  esp8266.begin(115200); // your esp's baud rate might be different
//  pinMode(buttonPin, INPUT);
   sensors.begin();
}

void loop() {
Serial.println("Loop Begin ");
Serial.println(esp8266.available());

if(esp8266.available())
{
Serial.println("inside esp8266  available");
// Calculation of weight 

val = 0.5 * val    +   0.5 * cell.read(); // take recent average
val2= ((val - 8326886)/29521.0f)*80 ;
Serial.println(((val - 8326886)/29521.0f)*80);
  
// Calculation of weight Ends

// Calculation of level
    Serial.println("beginning digital write trigPin low");
	digitalWrite(trigPin, LOW);
	delayMicroseconds(2);
	digitalWrite(trigPin, HIGH);
	delayMicroseconds(10);
	digitalWrite(trigPin, LOW);
	duration = pulseIn(echoPin, HIGH);
	distance= duration*0.034/2;
	
// Calculation of level Ends
	
// Calculation of Temprature Starts

  Serial.print("Requesting Temperatures from sensors: ");
  sensors.requestTemperatures(); 
  Serial.println("DONE");
  
  temperature = sensors.getTempCByIndex(0);

  Serial.print("Temperature is ");
  Serial.print(temperature);
  
// Calculation of Temprature Ends

// Sending Data Starts
        
          esp8266.print("<");
          esp8266.print(temperature);
          //esp8266.print("55");
          esp8266.print(":");
          esp8266.print(distance);
          //esp8266.print("55"); 
          esp8266.print(":");
          esp8266.print(val2);
          //esp8266.print("55");		  
          esp8266.print(">");
      
		  Serial.print("<");
          Serial.print(temperature);
          Serial.print(":");
          Serial.print(distance);
          Serial.print(":");
          Serial.print(val2);			  
          Serial.print(">");  
      delay(10000);

// Sending Data Ends
  }
}
